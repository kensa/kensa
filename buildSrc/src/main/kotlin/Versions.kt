import org.gradle.api.JavaVersion

object Versions {

    const val kotlin = "1.6.10"
    val mainJavaVersion = JavaVersion.VERSION_1_8
    val testJavaVersion = JavaVersion.VERSION_17

    // Wrapper
    const val gradleWrapper = "6.8.1"

    // Node
    const val node = "17.3.0"

    // Plugins
    const val moowork = "2.2.0"

    // Dependencies
    const val plantUml = "1.2020.23"
    const val pebble = "3.0.8"
    const val mockito = "4.0.0"
    const val kotest = "5.1.0"
    const val junitPlatform = "1.8.2"
    const val junitJupiter = "5.8.2"
    const val assertJ = "3.22.0"
    const val hamcrest = "1.3"
    const val awaitility = "4.0.3"
    const val minimalJson = "0.9.5"
    const val antlr = "4.8-1"
}